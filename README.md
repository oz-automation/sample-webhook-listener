# Sample GitLab Project

This sample project shows how a project in GitLab looks for demonstration purposes. It contains issues, merge requests and Markdown files in many branches,
named and filled with lorem ipsum.

You can look around to get an idea how to structure your project and, when done, you can safely delete this project.

[Learn more about creating GitLab projects.](https://docs.gitlab.com/ee/gitlab-basics/create-project.html)

## sample input

* [How to listen for webhooks using python](https://hackernoon.com/how-to-listen-for-webhooks-using-python-7g153uad)
* [intro to webhooks and how to receive them with python](https://www.realpythonproject.com/intro-to-webhooks-with-and-how-to-receive-them-with-python/)
* [pypi webhook listener](https://pypi.org/project/Webhook-Listener/)
* [create a simple webhook listener using python](https://forum.uipath.com/t/creating-a-simple-webhook-listener-using-python/235217)
* [how to receive webhooks in python with flask or django](https://blog.logrocket.com/receive-webhooks-python-flask-or-django/)

## how to use

~~~sh
export UCD_TOKEN='xxxxxx-yyyy-dddd-7777-qaqaqaq'
export UCD_SERVER="https://myucdserver:8443"
export UCD_COMPONENT="ucdtestcomponent"
python3 app.py
~~~

~~~sh
curl -x POST http://localhost:8000/webhook
~~~
