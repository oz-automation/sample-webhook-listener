# app.py

from flask import Flask, request
import requests, os
from requests.auth import HTTPBasicAuth

app = Flask(__name__)

def trigger_import():
    ucd_token=os.getenv("UCD_TOKEN")
    ucd_server=os.getenv("UCD_SERVER")
    ucd_component=os.getenv("UCD_COMPONENT")
    response = requests.put(f'{ucd_server}/cli/component/integrate',
            data = "{'component': '" + ucd_component + "', 'properties': ''}", auth=HTTPBasicAuth('PasswordIsAuthToken',ucd_token), verify=False)
    print("RESPONSE=")
    print (response.content)
    return str(response.status_code)

@app.route('/webhook', methods=['POST'])
def webhook():
    if request.method == 'POST':
        print("Data received from Webhook is: ", request.json)
        return trigger_import()

app.run(host='0.0.0.0', port=8000)